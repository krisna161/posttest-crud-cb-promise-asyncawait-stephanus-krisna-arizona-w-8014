const express = require('express');
const app = express();
const port = process.env.PORT || 6000;

const { Todo,User } = require('./models')

app.use(express.json())

app.get('/',(req,res) => {
    res.json({
        message: "Hello Selamat Datang di Posttest dengan Promise"
    })
})

//endpoint Todos
app.get('/todos', (req,res) => {
    Todo.findAll().then(todo => {
        res.status(200).json({
            info: todo
        })
    })
})

app.get('/todo/:id', (req, res) => {
  Todo.findByPk(req.params.id).then(todo => {
    res.status(200).json({
      status: true,
      message: `Todos with ID ${req.params.id} retrieved!`,
      data: todo
    })
  })
})

app.post('/todos', (req, res) => {
  Todo.create({
    name: req.body.name,
    description: req.body.description,
    due_at: req.body.due_at,
    user_id: req.body.user_id
  }).then(todo => {
    res.status(201).json({
      status: true,
      message: 'Todos created!',
      data: todo 
    })
  })
})

app.put('/todos/:id', (req, res) => {
  Todo.findByPk(req.params.id).then(todo => {
    todo.update({
        name: req.body.name,
        description: req.body.description,
        due_at: req.body.due_at,
        user_id: req.body.user_id
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `Todos with ID ${req.params.id} updated!`,
        data: todo 
      })
    })
  })
})

app.delete('/todos/:id', (req, res) => {
  Todo.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

//endpoint Users
app.get('/users', (req,res) => {
    User.findAll().then(user => {
        res.status(200).json({
            info: user
        })
    })
})

app.get('/users/:id', (req, res) => {
  User.findByPk(req.params.id).then(user => {
    res.status(200).json({
      status: true,
      message: `Users with ID ${req.params.id} retrieved!`,
      data: user
    })
  })
})

app.post('/users', (req, res) => {
  User.create({
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }).then(user => {
    res.status(201).json({
      status: true,
      message: 'Users created!',
      data: user 
    })
  })
})

app.put('/users/:id', (req, res) => {
  User.findByPk(req.params.id).then(user => {
    user.update({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
    }, {
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(200).json({
        status: true,
        message: `Users with ID ${req.params.id} updated!`,
        data: user 
      })
    })
  })
})

app.delete('/Users/:id', (req, res) => {
  User.destroy({
    where: {
      id: req.params.id
    }
  }).then(() => {
    res.status(204).end()
  })
})

app.listen(port, () => console.log(`Listening on port ${port}!`))