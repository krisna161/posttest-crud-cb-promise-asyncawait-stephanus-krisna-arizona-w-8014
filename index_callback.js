const express = require("express");
const app = express();
const port = process.env.PORT || 5001;

app.use(express.json());

const { Todo,User } = require('./models');

//controller 
function getAllTodo(req, res, next){
    try{
        Todo.findAll().then(todo => {
            res.status(200).json({
                status: true,
                message: 'Todo retreived',
                todo
            })
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}
function getTodoById(req, res, next){
    try{
        Todo.findByPk(req.params.id).then(todo => {
            if (todo === null) {
                res.status(202).json({
                    status: false,
                    message: `Todo with ID ${req.params.id} not Found!`,
                    todo
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `Todo with ID ${req.params.id} retrieved!`,
                    data: todo
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function createOneTodo(req,res,next) {
    try{
        Todo.findOne({ where: {name: req.body.name}}).then(todo => {
            if (todo){
                res.status(201).json({
                    status: false,
                    message: `Create todo failed due to ${req.body.name} already exist`
                })
            } else {
                Todo.create({
                    name: req.body.name,
                    description: req.body.description,
                    due_at: req.body.due_at,
                    user_id: req.body.user_id
                }).then(todo => {
                    res.status(201).json({
                        status: true,
                        message: `Todo with ID ${req.body.id} created`,
                        todo
                    })
                })
            }
        })
    } 
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function updateTodo(req, res, next){
   try {
    Todo.findByPk(req.params.id).then(todo => {
        if (todo === null) {
            res.status(200).json({
                status: false,
                message: `Todo ID ${req.params.id} not found`,
                todo
            })
        } else {
            Todo.update({
                name: req.body.name,
                description: req.body.description,
                due_at: req.body.due_at,
                user_id: req.body.user_id
            }, {
                where: {
                    id: req.params.id
                }
            }).then(user => {
                res.status(200).json({
                    status: true,
                    message: `Todo with ID ${req.params.id} updated!`,
                    todo
                })
            }) 
        }
    })
}
catch(err){
    res.status(422).json({
      status: false,
      message: err.message
  })
}
}
 
function deleteOneTodo(req, res, next){
    try{
        Todo.findOne({ where: {id: req.params.id}}).then(todo => {
            if (todo === null){
                res.status(201).json({
                    status: false,
                    message: `Todo ID ${req.params.id} does not exist`
                })
            } else {
                Todo.destroy({ where: {id: req.params.id}}).then(todo => {
                    res.status(200).json({
                        status: true,
                        message: `Todo ID ${req.params.id} deleted`
                    })
                })
            }
           })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
      })
    }
}

//controller User
function getAllUser(req, res, next){
    try{
        User.findAll().then(user => {
            res.status(200).json({
                status: true,
                message: 'Todo retreived',
                user
            })
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

async function getUserByIdWithTodo(req,res,next) {
    try {
        const user = await User.findOne({
            where: { id: req.params.id },
            include: [{
            model: user
            }]
            });
            if (user === null) {
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user
                })
            }
    } catch (err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function getUserById(req, res, next){
    try{
        User.findByPk(req.params.id).then(user => {
            if (user === null) {
                res.status(202).json({
                    status: false,
                    message: `User with ID ${req.params.id} not Found!`,
                    user
                })
            } else {
                res.status(200).json({
                    status: false,
                    message: `User with ID ${req.params.id} retrieved!`,
                    user
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function createOneUser(req,res,next) {
    try{
        User.findOne({ where: {email: req.body.email}}).then(user => {
            if (user){
                res.status(201).json({
                    status: false,
                    message: `Create user failed due to ${req.body.email} already exist`
                })
            } else {
                User.create({
                    name: req.body.name,
                    email: req.body.email,
                    password: req.body.password
                }).then(user => {
                    res.status(201).json({
                        status: true,
                        message: `User with ID ${req.body.id} created`,
                        user
                    })
                })
            }
        })
    } 
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

function updateUser(req, res, next){
   try {
    User.findByPk(req.params.id).then(user => {
        if (user === null) {
            res.status(200).json({
                status: false,
                message: `User ID ${req.params.id} not found`,
                user
            })
        } else {
            User.update({
                name: req.body.name,
                email: req.body.email,
                password: req.body.password
            }, {
                where: {
                    id: req.params.id
                }
            }).then(user => {
                res.status(200).json({
                    status: true,
                    message: `User with ID ${req.params.id} updated!`,
                    user
                })
            }) 
        }
    })
}
catch(err){
    res.status(422).json({
      status: false,
      message: err.message
  })
}
}
 
function deleteOneUser(req, res, next){
    try{
        User.findOne({ where: {id: req.params.id}}).then(user => {
            if (user === null){
                res.status(201).json({
                    status: false,
                    message: `User ID ${req.params.id} does not exist`
                })
            } else {
                User.destroy({ where: {id: req.params.id}}).then(user => {
                    res.status(200).json({
                        status: true,
                        message: `User ID ${req.params.id} deleted`
                    })
                })
            }
           })
    }
    catch(err){
        res.status(422).json({
          status: false,
          message: err.message
      })
    }
}





module.exports = {
    getAllTodo,
    getTodoById,
    updateTodo,
    deleteOneTodo,
    createOneTodo,
    getAllUser,
    getUserById,
    updateUser,
    deleteOneUser,
    createOneUser,
    getUserByIdWithTodo
}


app.get('/',(req,res) => {
    res.json({
        message: "Hello Selamat Datang di Posttest dengan Callback"
    })
})


//Endpoint Todo
app.get('/todos', function(req, res) {
    try{
        getAllTodo(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.get('/todo/:id', function(req, res) {
    try{
        getTodoById(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.post('/todos', function(req, res) {
    try{
        createOneTodo(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.put('/todo/:id', function(req, res) {
    try{
        updateTodo(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.delete('/todos/:id', function(req, res) {
    try{
        deleteOneTodo(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

//Endpoint User
app.get('/users', function(req, res) {
    try{
        getAllUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.get('/users/:id', function(req, res) {
    try{
        getUserByIdWithTodo(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.post('/users', function(req, res) {
    try{
        createOneUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})
app.put('/users/:id', function(req, res) {
    try{
        updateUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.delete('/users/:id', function(req, res) {
    try{
        deleteOneUser(req, res, function(error, user){
            if (!error){
                res.status(200).json({
                    status: true,
                    message: 'OK',
                    user
                })
            } else {
                res.status(202).json({
                    status: false,
                    message: 'Error'
                })
            }
        })
    }
    catch(err){
        res.status(422).json({
        status: false,
        message: err.message
        })
    }
})

app.listen(port, () => console.log(`Listen on port ${port}`));
